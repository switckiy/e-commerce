<?php

namespace App\Controllers;

use App\Models\ShopModel;

helper('Myth\Auth\Helpers\auth');

class Home extends BaseController
{
    public function index()
    {
        return view('shop/index');
    }
    public function contact()
    {
        return view('shop/contact');
    }
    public function shop()
    {
        $shopModel = new ShopModel();
        $data['products'] = $shopModel->findAll();

        return view('shop/shop', $data);
    }
    public function about()
    {
        return view('shop/about');
    }
    public function detile($id)
    {

        $productModel = new ShopModel();
        // Create an instance of the product model


        // Fetch the product data from the model based on the $id
        $productData = $productModel->find($id);

        // Check if the product data is available
        if ($productData) {
            // Pass the product data to the view
            $data = [
                'productId' => $productData['id'],
                'images' => $productData['images'],
                'productName' => $productData['name'],
                'productDescription' => $productData['deskripsi'],
                'productPrice' => $productData['price']
            ];
            $datas = array(user_id());

            // Load the view and pass the data
            return view('shop/detailes', $data, $datas);
        } else {
            // Handle the case when the product data is not found
            // Redirect or show an error message
        }
    }


    public function cart()
    {
        $data = user();
        return view('shop/cart', [$data]);
    }

    public function addToCart($productId)
    {
        $quantity = $this->request->getPost('quantity');
        // Validasi inputan, misalnya pastikan $productId dan $quantity valid


        $user = array(user_id());

        // Ambil data produk dari model
        $productModel = new ShopModel();
        $product = $productModel->find($productId);

        // Simpan data ke dalam keranjang belanja (bisa disimpan ke session, database, atau tempat lain)
        $cartItem = [
            'user' => $user,
            'product' => $product,
            'quantity' => $quantity
        ];

        // Tampilkan view dengan data keranjang belanja
        return view('shop/cart', ['cartItem' => $cartItem]);
    }
}
